package com.example.examenc1;

import android.os.Bundle;
import android.text.InputType;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import androidx.appcompat.app.AppCompatActivity;

public class RectanguloActivity extends AppCompatActivity {

    private EditText etBase, etAltura;
    private TextView tvArea, tvPerimetro;
    private Button buttonCalcular, buttonLimpiar, buttonRegresar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rectangulo);

        etBase = findViewById(R.id.etBase);
        etAltura = findViewById(R.id.etAltura);
        tvArea = findViewById(R.id.tvArea);
        tvPerimetro = findViewById(R.id.tvPerimetro);
        buttonCalcular = findViewById(R.id.buttonCalcular);
        buttonLimpiar = findViewById(R.id.buttonLimpiar);
        buttonRegresar = findViewById(R.id.buttonRegresar);

        // Obtener el nombre del usuario del Intent
        String nombreUsuario = getIntent().getStringExtra("nombreUsuario");
        TextView tvNombre = findViewById(R.id.tvNombre);
        tvNombre.setText("" + nombreUsuario);

        // Restringir la entrada de letras en etBase y etAltura
        etBase.setInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_NUMBER_FLAG_DECIMAL);
        etAltura.setInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_NUMBER_FLAG_DECIMAL);

        buttonCalcular.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                calcularResultados();
            }
        });

        buttonLimpiar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                limpiarCampos();
            }
        });

        buttonRegresar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    private void calcularResultados() {
        String baseStr = etBase.getText().toString();
        String alturaStr = etAltura.getText().toString();

        if (!baseStr.isEmpty() && !alturaStr.isEmpty()) {
            double base = Double.parseDouble(baseStr);
            double altura = Double.parseDouble(alturaStr);
            double area = base * altura;
            double perimetro = 2 * (base + altura);

            tvArea.setText("Área: " + area);
            tvPerimetro.setText("Perímetro: " + perimetro);
        } else {
            tvArea.setText("Por favor ingrese la base y la altura.");
            tvPerimetro.setText("");
        }
    }

    private void limpiarCampos() {
        etBase.setText("");
        etAltura.setText("");
        tvArea.setText("Área");
        tvPerimetro.setText("Perímetro");
    }
}
